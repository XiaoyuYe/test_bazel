#include "stdio.h"
#include "mediapipe/framework/calculator_framework.h"
#include "mediapipe/framework/formats/image_frame.h"
#include "mediapipe/framework/formats/image_frame_opencv.h"
#include "mediapipe/framework/formats/detection.pb.h"
#include <sstream>
#include <fstream>

namespace mediapipe
{
  class OfflineTranslationCalculator : public CalculatorBase
  {
  public:
    OfflineTranslationCalculator(){};
    ~OfflineTranslationCalculator(){};
    static ::mediapipe::Status GetContract(CalculatorContract *cc)
    {
        LOG(INFO) << "GetContract(CalculatorContract *cc)";
        cc->Inputs().Tag("DETECTIONS").Set<std::vector<Detection>>();
        cc->Outputs().Tag("DETECTIONS").Set<std::vector<Detection>>();

        // LOG(INFO) << "finishes getcontract";
        return ::mediapipe::OkStatus();
    }
    ::mediapipe::Status Open(CalculatorContext *cc)
    {
      LOG(INFO) << "Open(CalculatorContext *cc)";
      cc->SetOffset(TimestampDiff(0));
      return ::mediapipe::OkStatus();
    }
    ::mediapipe::Status Process(CalculatorContext *cc)
    {
      LOG(INFO) << "Process(CalculatorContext *cc)";
    
      const auto& input_detections = cc->Inputs().Tag("DETECTIONS").Get<std::vector<Detection>>();
      auto output_detections = absl::make_unique<std::vector<Detection>>();

      for (int i = 0; i < 1; i++) {
          Detection detection;
          detection.add_score(0.5f);
          detection.add_label("你好世界");

          LocationData* location_data = detection.mutable_location_data();
          location_data->set_format(LocationData::RELATIVE_BOUNDING_BOX);

          LocationData::RelativeBoundingBox* relative_bbox =
              location_data->mutable_relative_bounding_box();
          // auto rect = cv::boundingRect(results.items.at(i).polygon);

          relative_bbox->set_xmin(0.25f);
          relative_bbox->set_ymin(0.1f);
          relative_bbox->set_width(0.25f);
          relative_bbox->set_height(0.25f);
          output_detections->emplace_back(detection);
      }

      // Output
      if (cc->Outputs().HasTag("DETECTIONS")) {
          cc->Outputs()
          .Tag("DETECTIONS")
          .Add(output_detections.release(), cc->InputTimestamp());
      }

      return ::mediapipe::OkStatus();
    }
    ::mediapipe::Status Close(CalculatorContext *cc)
    {
      LOG(INFO) << "Close(CalculatorContext *cc)";
      // detector.release();
      return ::mediapipe::OkStatus();
    }
  };
  REGISTER_CALCULATOR(OfflineTranslationCalculator);
}
