#include "stdio.h"
#include "mediapipe/framework/calculator_framework.h"
// #include "oua_ocr_0225/oua_ocr_0225/ocr.h"
#include "mediapipe/framework/formats/image_frame.h"
#include "mediapipe/framework/formats/image_frame_opencv.h"
#include "mediapipe/framework/formats/detection.pb.h"
#include <sstream>
#include <fstream>

namespace mediapipe
{
    class OfflineOCRCalculator : public CalculatorBase
    {
    private:
        // std::unique_ptr<oua::OCR> detector;
        // std::string* load_files(std::string filepath) {
        //     std::ifstream file(filepath, std::ifstream::binary | std::ifstream::in);
        //     std::stringstream buffer;
        //     buffer << file.rdbuf();
        //     return new std::string(buffer.str());
        // }
        // u_long time_detection(std::string image_path, std::unique_ptr<oua::OCR> &detector) {
        //     cv::Mat img = cv::imread(image_path, cv::ImreadModes::IMREAD_COLOR);
        //     // LOG(INFO) << "loads " + image_path;
        //     oua::OCRResult result;
        //     result.textArea = std::vector<cv::Point>();
        //     result.items = std::vector<oua::OCRItem>();
        //     auto start = std::chrono::high_resolution_clock::now();
        //     detector->run(img, result);
        //     auto end = std::chrono::high_resolution_clock::now();
        //     auto duration = duration_cast<std::chrono::milliseconds>(end - start);
        //     // LOG(INFO) << "runs detection";
        //     return duration.count();
        // }
    public:
        OfflineOCRCalculator(){};
        ~OfflineOCRCalculator(){};
        static ::mediapipe::Status GetContract(CalculatorContract *cc)
        {
            LOG(INFO) << "GetContract(CalculatorContract *cc)";
            cc->Inputs().Tag("IMAGE").Set<ImageFrame>();
            cc->Outputs().Tag("DETECTIONS").Set<std::vector<Detection>>();
            return ::mediapipe::OkStatus();
        }
        ::mediapipe::Status Open(CalculatorContext *cc)
        {
            LOG(INFO) << "Open(CalculatorContext *cc)";
            cc->SetOffset(TimestampDiff(0));
            return ::mediapipe::OkStatus();
        }
        ::mediapipe::Status Process(CalculatorContext *cc)
        {
            LOG(INFO) << "Process(CalculatorContext *cc)";
          
            auto output_detections = absl::make_unique<std::vector<Detection>>();

            if (!cc->Inputs().Tag("IMAGE").IsEmpty())
            {
                const auto &input = cc->Inputs().Tag("IMAGE").Get<ImageFrame>();
                cv::Mat frame = ::mediapipe::formats::MatView(&input);
                // oua::OCRResult results;
                // results.textArea = std::vector<cv::Point>();
                // results.items = std::vector<oua::OCRItem>();
    
                // detector->run(frame, results);
                for (int i = 0; i < 1; i++) {
                    Detection detection;
                    detection.add_score(0.5f);
                    detection.add_label("Hello World");

                    LocationData* location_data = detection.mutable_location_data();
                    location_data->set_format(LocationData::RELATIVE_BOUNDING_BOX);

                    LocationData::RelativeBoundingBox* relative_bbox =
                        location_data->mutable_relative_bounding_box();
                    // auto rect = cv::boundingRect(results.items.at(i).polygon);

                    relative_bbox->set_xmin(0.25f);
                    relative_bbox->set_ymin(0.1f);
                    relative_bbox->set_width(0.25f);
                    relative_bbox->set_height(0.25f);
                    output_detections->emplace_back(detection);
                }

                LOG(INFO) << "OCRResult to std::vector<Detection> works";
            }

            // Output
            if (cc->Outputs().HasTag("DETECTIONS")) {
                cc->Outputs()
                .Tag("DETECTIONS")
                .Add(output_detections.release(), cc->InputTimestamp());
            }

            LOG(INFO) << "mediapipe::OkStatus()";
            return ::mediapipe::OkStatus();
        }
        ::mediapipe::Status Close(CalculatorContext *cc)
        {
            LOG(INFO) << "Close(CalculatorContext *cc)";
            // detector.release();
            return ::mediapipe::OkStatus();
        }
    };
    REGISTER_CALCULATOR(OfflineOCRCalculator);
}
